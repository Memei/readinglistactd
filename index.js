// console.log("Hello World");
//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:

/*Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.*/

/*1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)*/

    function getAverage(...numbers){
    let x = 1;
    let sum = 0;

    for(const number of numbers){
        sum += number;
        x++;
    }

    let score = Math.round(sum / (x - 1));
    console.log(score);

    if(score <= 80){
        console.log("Your score is " + score + ". You will proceed to Grade 10 Section Opal");
    }else if(score >= 81 && score <= 120){
        console.log("Your score is " + score + ". You will proceed to Grade 10 Section Opal");

    }else if(score >= 121 && score <= 160){
        console.log("Your score is " + score + ". You will proceed to Grade 10 Section Sapphire");

    }else if(score >= 161 && score <= 200){
        console.log("Your score is " + score + ". You will proceed to Grade 10 Section Diamond");
    }
}

getAverage(72, 63, 93, 89);
getAverage(75, 75, 93, 89);
getAverage(100, 127, 128, 128);
getAverage(200, 200, 200, 189);

/*2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/

function longestWord(string) {
    var str = string.split(" ");
    var longest = 0;
    var word = null;
    str.forEach(function(str) {
        if (longest < str.length) {
            longest = str.length;
            word = str;
        }
    });
    return word;
}
console.log("Longest word:")
console.log(longestWord("Web Development Tutorial"));

/*3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'*/

function firstNonRepeatedCharacter(string) {
    return string.split('').filter(function (character, index, obj) {
        return obj.indexOf(character) === obj.lastIndexOf(character);
    }).shift();
}

console.log("First not repeated character:");
console.log(firstNonRepeatedCharacter('abacddbec'));


//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/

let http = require('http');

let products = [
    {
        "name": 'iPhone 13 Pro Max',
        "description": 'New iPhone',
        "price": 60000,
        "stock": 25
    },
    {

        "name": 'iPhone 14 Pro Max',
        "description": 'Newest iPhone',
        "price": 80000,
        "stock": 11
    }
];

let port = 8000

let server = http.createServer(function(request, response) {
    if(request.url == '/' && request.method == 'GET') {
        response.writeHead(200, {'Content-Type': 'application/json'})
        response.end('Welcome to Ordering System!')
    } else if(request.url == '/dashboard' && request.method == 'GET') {
        response.writeHead(200, {'Content-Type': 'application/json'})
        response.end("Welcome to your User's Dashboard!")
    } else if(request.url == '/products' && request.method == 'GET') {
        response.writeHead(200, {'Content-Type': 'application/json'})
        response.write("Here’s our products available")
        response.write(JSON.stringify(products))
        response.end()
    } else if(request.url == '/addProduct' && request.method == 'POST') {
        let request_body = ''

        request.on('data', function(data) {
            request_body += data
        })
        request.on('end', function() {
            console.log(typeof request_body)
            request_body = JSON.parse(request_body)

            let new_product = {
                "name": request_body.name,
                "description": request_body.description,
                "price": request_body.price,
                "stock": request_body.stock
            }

            products.push(new_product)
            console.log(products)

            response.writeHead(200, {'Content-Type': 'application/json'})
            response.write(JSON.stringify(new_product))
            response.end()
        })

    } else if(request.url = "/updateProduct" && request.method == "PUT"){

                response.writeHead(200, {'Content-Type': 'text/plain'});
                response.end('Update a product to our resources');

    } else if(request.url = "/archiveProduct" && request.method == "DELETE"){

                    response.writeHead(200, {'Content-Type': 'text/plain'});
                    response.end('Archive products to our resources');

    }

});

server.listen(port);

console.log(`Server is running on localhost: ${port}`);

